##################################################
############## R�seau de p�tri ###################
##################################################

To launch the network you have to run the class main in the file src in the package org.pneditor.editor.
(you must run it as a JAVA application). To use our model you should choose the model 
lilianclement. 

###### What is a petri-net ? ######

A Petri net, also known as a place/transition (PT) net, is a 
mathematical modelling language that can be used to describe a 
distributed system. It is a class of discrete event dynamic systems. 
A Petri net is a directed bipartite graph that has two types of elements, 
places and transitions, depicted respectively as white circles and white squares. 
A place can contain any number of tokens, depicted as 
black circles inside the place (when there are more than 9 tokens the 
number of tokens is directly written in the place). A transition is 
enabled if all places connected to it as inputs contain at least one token. 
Some sources say that Petri nets were invented in August 1939 by Carl Adam 
Petri at the age of 13 to describe chemical processes.


#######   The graphic interface  #######

The graphical interface was already coded. It represents the places, the arc 
and the token the way it is described before. It removes the arc if you set
its multiplicity to zero or with a negative number. The arcs zeros are represented 
with a little circle at the end of them and the reset arcs are represented with a
double arrow. If a transition is not possible it has a red colour and if it is possible 
it has a green colour. 


#######  Our model of petri net #######

Our model is slightly different from the initial model. Places and transitions
are linked with arcs, those arcs can have weight (or multiplicity), which indicates
 the numbers of tokens which should be present in a place for the transition to be 
enabled. It also indicates the number of tokens which will be removed from the place. 
Moreover, there are two types of Arc, arcs which are "videur" or reset arc, and 
arcs which are zeros or inhibitory. If an arc is a reset arc it will empty the place 
of all its tokens each time the transition is done. If an arc is zero, it means that 
the transition is possible only when the place has zero tokens. You can add a place, 
a transition and an arc and remove each of them. You can change the multiplicity of 
an arc and set the number of tokens of a place. 

It while raise an error message if you try to set a negative number of tokens in a place, 
it is also forbidden for an arc to have a negative multiplicity (it will remove it from 
the graphic interface). 

If you create an arc between a place and a transition that is already linked, it will 
create an arc with a multiplicity which is the sum of the multiplicity of the two arcs.

In our model, the classes of Arc_zero and Arc_videur extends the class Arc. 
Another aspect of our model is that each element of the net (place, transition and arcs) 
have a unique name. To describe the direction of an Arc, we use a boolean : 
if an arc is entering a Transition, the boolean is true, if it is leaving the boolean is false. 


#######  The adapter #######

To integrate our model in the graphic interface, we used the pattern adapter, with 
a PetriNetAdapter and the classes PlaceAdapter, ArcAdapter and TransitionAdapter which 
can generate an AbstractPlace, AbstractTransition , AbstractArc which are used in the 
graphic interface. 
We had to make some choices to adapt our model : in our model a reset arc has a multiplicity 
of -1, but here it generated an error so we defined it with a multiplicity of 100000. Also 
the name of the places, transitions and arcs are imposed and are numbers that are incremented
each time you create an item. 


#######  Some issues #######

There are some issues with our adapter and the graphic interface ; we didn't know how to remove
them and we did not have time to do so. Firstly, if you remove a place or a transition, the graphic 
interface does not remove the arcs which are linked to them. You should first remove the 
arc then the place or the transition if you want them to disappear from the graphic interface.
Indeed, even if it stays on the graphic interface, it is really removed from the set of arc 
known by the net.
