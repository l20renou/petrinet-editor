package org.pneditor.petrinet.models.imta;
import org.pneditor.petrinet.models.lilianclement.exceptions.AlreadyExistingName;
import org.pneditor.petrinet.models.lilianclement.exceptions.DoesNotExist;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeToken;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeWeight;
import org.pneditor.petrinet.models.lilianclement.exceptions.LeavingArc;

public class ArcVideur extends Arc{

  
	public ArcVideur (String name, int weight, boolean isEntering, Transition transition, Place place) throws NegativeWeight{
		super(name, weight, isEntering, transition, place);
	}
	
	/**This method returns true if there is a token in the place corresponding to the arc
	 * @return a boolean
	 * @throws an exception if the arc is getting in the place
	 */
	
	public boolean isPossible() throws LeavingArc {
		if (!this.getType()) {
			throw new LeavingArc();
		}
		else {
			return this.getPlace().getToken()>0;
		}
		
	}
	
	/**
	 * Makes the transition for one arc, assuming that this one is possible
	 * @throws LeavingArc 
	 */
	
	public void changeToken1Arc() throws NegativeToken{
		if (this.getType()) {
			// true means that the arc is getting out of the place
			this.getPlace().setNbToken(0); 
		}
		//if the arc is leaving the transition, we don't change the place it is
		//linked to
	}
	
}
