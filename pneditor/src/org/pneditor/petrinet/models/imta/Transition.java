package org.pneditor.petrinet.models.imta;
import java.util.LinkedList;


import org.pneditor.petrinet.models.lilianclement.exceptions.DoesNotExist;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeToken;

import org.pneditor.petrinet.models.lilianclement.exceptions.LeavingArc;

public class Transition {
	
	private String name;
	
	public Transition(String name) {
		this.name = name;
		this.arcsTransition = new LinkedList<Arc>();
	}
	
	public String getName() {
		return this.name;
	}
	
	private LinkedList<Arc> arcsTransition; 
	
	/**
	 * This methods gives the list of the arcs going out of the transition
	 * @return a LinkedList<Arc>
	 */
	
	public LinkedList<Arc> getArcsLeaving () {
		LinkedList<Arc> ArcsLeaving = new LinkedList<Arc>(); 
		for (Arc A : this.arcsTransition) {
			if (!A.getType()) {
				ArcsLeaving.add(A); 
			}
		 }
		return ArcsLeaving;
	}
	
	/**
	 * This methods gives the list of the arcs going in the transition
	 * @return a LinkedList<Arc>
	 */
	
	public LinkedList<Arc> getArcEntering () {
		LinkedList<Arc> ArcsEntering = new LinkedList<Arc>(); 
		for (Arc A : this.arcsTransition) {
			if (A.getType()) {
				ArcsEntering.add(A); 
			}
		}
		return ArcsEntering;		
	}
	
	/**
	 * Method that gives the list of the arcs linked to the transition
	 * @return LinkedList<Arc>
	 */
	
	public LinkedList<Arc> getArcs() {
		return this.arcsTransition;
	}
	
	/**
	 * Adds a transition to the list of transitions
	 * @param arc
	 */
	
	public void addArcTransition(Arc arc) {
		arcsTransition.add(arc);
	}
	
	/**
	 * Delete an arc from the list of arcTransitions
	 * @param arc
	 * @throws DoesNotExist
	 */
	
	public void removeArcTransition(Arc arc) throws DoesNotExist{
		boolean condition = true;
		for (int i = 0 ; i < arcsTransition.size(); i++) {
			if (arc.equals(arcsTransition.get(i))) {
				arcsTransition.remove(i);
				condition = false;
			}
		}
		if (condition) {	
			throw new DoesNotExist("This arc doesn't exist") ;
			}	
	}
	
	/**
	 * This method determines if the transition is tirable
	 * @return a boolean
	 * @throws LeavingArc
	 */
	
	public boolean isTirable () throws LeavingArc{
		boolean condition = true;
		for (Arc A : this.getArcEntering()) {
			if (!A.isPossible()) {
				condition = false; 
			}
		}
		return condition; 
	}
		
		
	
	/**
	 * This method makes the transition : it passes all the tokens from the entering places to the 
	 * outgoing ones.
	 * We don't have to test if the transition is tirable, as the test is already done in the function fire
	 * @throws LeavingArc
	 */
	
	public void realiseTransition () throws LeavingArc, NegativeToken {
			for (Arc A : this.arcsTransition) {
				A.changeToken1Arc();
			}
	}
	
	
	/**
	 * Implements the method equals. It only verify the name, as we assume that two differents objects
	 * will not have the same name.
	 */
	
	public boolean equals(Object o){
		if (o == null ||!(o instanceof Transition)) return false;
		Transition t = (Transition) o;	
		return(this.name.equals(t.getName())); //we only test the name and not all the arguments
	}
	
	/**
	 * The classical method toString()
	 */
	
	public String toString() {
		String newLine = System.getProperty("line.separator");
		return ("Name : " + this.name + ", List of arcs : " + this.arcsTransition.toString()+ newLine);
	}
}