package org.pneditor.petrinet.models.imta;
import org.pneditor.petrinet.models.lilianclement.exceptions.AlreadyExistingName;
import org.pneditor.petrinet.models.lilianclement.exceptions.DoesNotExist;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeToken;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeWeight;
import org.pneditor.petrinet.models.lilianclement.exceptions.LeavingArc;

public class ArcZero extends Arc {

	public ArcZero (String name, int weight, boolean isEntering, Transition transition, Place place) throws NegativeWeight {
		super(name, weight, isEntering, transition, place);
	}
	
	
	/**This method return true if the tokens can pass this arc, which means that the corresponding place is empty 
	 * @return a boolean
	 * @throws an exception if the arc is sorting
	 */
	
	public boolean isPossible() throws LeavingArc {
		if (!this.getType()) {
			throw new LeavingArc();
		}
		else {
			return this.getPlace().getToken()==0;
		}
	}
	
	/**
	 * No need to makes it change the tokens from a place, as it never changes the number of tokens from
	 * one place (either it is inactive, either the place has 0 tokens and therefore nothing happens)
	 */
	
	public void changeToken1Arc() {
	}	
}
