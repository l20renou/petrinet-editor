package org.pneditor.petrinet.models.imta;
import org.pneditor.petrinet.models.lilianclement.exceptions.AlreadyExistingName;
import org.pneditor.petrinet.models.lilianclement.exceptions.DoesNotExist;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeToken;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeWeight;
import org.pneditor.petrinet.models.lilianclement.exceptions.LeavingArc;

public class Place {
	
	private String name;
	private int nbToken;
	
	/**
	 * Constructor which create an abstract place
	 * @param str name
	 * @param int nbToken
	 * @throws NegativeToken
	 */
	public Place (String name, int nbToken) throws NegativeToken{
		this.name = name;
		if (nbToken<0) {
			throw new NegativeToken("The number of tokens can't be negative");
		}
		this.nbToken = nbToken;
	}
	
	/**
	 * Method to get tokens
	 * @return int nbToken
	 */
	public int getToken () {
		return this.nbToken ; 
	}
	
	/**
	 * Method to change the number of tokens
	 * @param int newNbToken
	 * @throws NegativeToken
	 */
	public void setNbToken(int newNbToken) throws NegativeToken {
		if (newNbToken <0) {
			throw new NegativeToken("The number of token can't be negative");
		}
		this.nbToken = newNbToken;
	}
	
	/**
	 * Method to getName
	 * @return string 
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Implements the method equals. It only verify the name, as we assume that two differents objects
	 * will not have the same name.
	 * @return boolean
	 */
	
	public boolean equals(Object o){
		if (o == null ||!(o instanceof Place)) return false;
		Place p = (Place) o;	
		return(this.name == p.getName());
	}
	/**
	 * Method toString 
	 * @return string 
	 */
	
	public String toString() {
		String newLine = System.getProperty("line.separator");
		return ("Name :" + this.name +","+ " Number of tokens :" + this.nbToken + newLine);
	}
}