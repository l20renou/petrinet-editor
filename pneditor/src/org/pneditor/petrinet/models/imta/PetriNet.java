package org.pneditor.petrinet.models.imta;
import java.util.LinkedList;
import java.lang.Math;
import org.pneditor.petrinet.models.lilianclement.exceptions.AlreadyExistingName;
import org.pneditor.petrinet.models.lilianclement.exceptions.DoesNotExist;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeToken;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeWeight;
import org.pneditor.petrinet.models.lilianclement.exceptions.LeavingArc;

public class PetriNet implements IPetriNet {
	
	private String name;
	private LinkedList<Arc> arcs; 
	private LinkedList<Place> places; 
	private LinkedList<Transition> transitions; 

	
	
	public PetriNet (String name) {
		this.name = name;
		this.arcs = new LinkedList();
		this.places = new LinkedList<Place>();
		this.transitions = new LinkedList<Transition>();
	}
	
	public LinkedList<Arc> getArcs () {
		return this.arcs;
	}
	
	public LinkedList<Place> getPlaces() {
		return this.places;
	}
	
	public LinkedList<Transition> getTransitions() {
		return this.transitions;
	}
		
	public void addTransition (String name) throws AlreadyExistingName {
		for (Transition T : this.transitions) {
			if(T.getName().equals(name)) {
				throw new AlreadyExistingName("The name already exist ");
			}
		}
		Transition transition = new Transition(name); 	
		transitions.add(transition);
	}
	
		
	public void addPlace(String name, int nbToken) throws AlreadyExistingName, NegativeToken {
		
		for (Place P : this.places) {
			if(P.getName().equals(name)) {
				throw new AlreadyExistingName("The name already exist ");				}
		}
		if (nbToken < 0) {
			throw new NegativeToken("The number of tokens can't be negative");
		}
		Place place = new Place(name, nbToken ); 	
		places.add(place);
	
	}
	
	/**
	 * Method which add an arc to the list of already existing arcs 
	 * and also add it to the list of arc in the transition it is linked with 
	 * @param arc
	 */
	
	public void addArc (String name, int weight, boolean isEntering, Transition transition, Place place) throws AlreadyExistingName, DoesNotExist, NegativeWeight{
		boolean condition = false ; 

		for (Arc A : this.arcs) {
			if(A.getName().equals(name)) {
				throw new AlreadyExistingName("The name already exist ");				}
			
			if(A.getTransition().equals(transition) && A.getPlace().equals(place) && A.getType() == isEntering) {
				int existingWeight = A.getWeight();
				A.setWeight(weight+existingWeight);
				condition = true ; 
			}
		}
		

		if (condition) {
			System.out.println("This arc olready exist, its weight is now the sum of both arcs");
		}
		
		else {
			Arc arc = new Arc(name, weight, isEntering, transition, place);
			if (weight> 0) {
				arcs.add(arc);
				arc.getTransition().addArcTransition(arc); // we add the new arc in the list of arcs of the class transition 
			}
			
			if (weight == -1) {
				ArcVideur arcVideur = new ArcVideur(name, weight, isEntering, transition, place);
				arcs.add(arcVideur);
				arcVideur.getTransition().addArcTransition(arcVideur); // we add the new arc in the list of arcs of the class transition 

			}
			else if (weight == 0) {
				ArcZero arcZero = new ArcZero(name, weight, isEntering, transition, place);
				arcs.add(arcZero);
				arcZero.getTransition().addArcTransition(arcZero); // we add the new arc in the list of arcs of the class transition 

			}
			
			else if (weight <-1){
				System.out.println("The weight is negative, it has been changed to it abolute value");
				arcs.add(arc);
				arc.getTransition().addArcTransition(arc); // we add the new arc in the list of arcs of the class transition 
			
			}
		}
		
	}
	
	/**
	 * Allows to set the number of tokens in a given place
	 * @param newNbTokens
	 * @param place
	 */
	
	public void setNbTokens(int newNbTokens, Place place) throws DoesNotExist, NegativeToken {
		boolean condition = true; 

		for (Place P : this.places) {
			if (place.equals(P)) {
				P.setNbToken(newNbTokens);
				condition = false; 
			}
		}
		if (condition) {
			throw new DoesNotExist("The place doesn't exist");
		}
		
	}
	
	/**
	 * This method allows to set the weight of an arc, gives an exception if the arc doesn't exist
	 * @param weight
	 * @param arc
	 * @throws DoesNotExist
	 * @throws NegativeWeight 
	 */
	
	public void setWeight(int weight, Arc arc) throws DoesNotExist, NegativeWeight {
		boolean condition = true; 
		
		for (Arc A : this.arcs) {
			if (arc.equals(A)) {
				A.setWeight(weight);
				condition= false ; 
			}
		}
		if (condition) {
			throw new DoesNotExist("The arc doesn't exist");
		}
	}
	
	/**
	 * This method allows to delete an arc, gives an exception if the arc doesn't exist. 
	 * @param place
	 * @throws DoesNotExist
	 */

	public void removeArc(Arc arc) throws DoesNotExist{
		for (int i = 0 ; i < arcs.size(); i++) {
			if (arc.equals(arcs.get(i))) {
				arcs.remove(i);
			}	
		}
		arc.getTransition().removeArcTransition(arc); //we delete the arc from the list of the arc in the class transition
	}
	
	/**
	 * This method allows to delete a transition.
	 * It also deletes all the arcs which were linked to this transition.
	 * @param place
	 * @throws DoesNotExist
*	 */
	
	
	public void removeTransition (Transition transition) throws DoesNotExist {
		while (!transition.getArcs().isEmpty()) {
			removeArc(transition.getArcs().get(0));
		}
		
		for (int i =0 ; i < transitions.size(); i++) {
			if (transitions.get(i).equals(transition)) {
				transitions.remove(i);
			}
		}
	}
	
	/**
	 * This method allows to delete a place.
	 * It also delete all the arcs which were linked to this place
	 * and it deletes the arcs which are present in the list of arcs known by transition 
	 * @param place
	 * @throws DoesNotExist
	 */
	
	public void removePlace (Place place) throws DoesNotExist{
		boolean condition = true;
		for (int i = 0 ; i < places.size(); i++) {
			if (place.equals(places.get(i))) {
				places.remove(i);
				condition = false;
			}	
		}
		for (int i = 0; i < arcs.size(); i++) {
			if (arcs.get(i).getPlace().equals(place)) {
				removeArc(arcs.get(i)); //we remove the arc linked to this place
			}
		}
		if (condition) {
			throw new DoesNotExist("The place does not exists");
		}
	}
	
	/**
	 * This method returns the transition with the corresponding name
	 * @return Transition
	 * @throws DoesNotExist
	 */
	
	public Transition getTransition(String name) throws DoesNotExist {
		for (Transition T : this.transitions) {
			if (T.getName().equals(name)) {
				return T;
			}
		}
		throw new DoesNotExist("The transition with this name doesn't exists");
	}
	
	/**
	 * This method returns the Arc with the corresponding name
	 * @return Arc
	 * @throws DoesNotExist
	 */
	
	public Arc getArc(String name) throws DoesNotExist {
		for (Arc A : this.arcs) {
			if (A.getName().equals(name)) {
				return A;
			}
		}
		throw new DoesNotExist("The Arc with this name doesn't exists");
	}
	
	/**
	 * This method returns the Place with the corresponding name
	 * @return Place
	 * @throws DoesNotExist
	 */
	
	public Place getPlace(String name) throws DoesNotExist {
		
		for (Place P : this.places) {
			if (P.getName().equals(name)) {
				return P;
			}
		}
		throw new DoesNotExist("The Place with this name doesn't exists");
	}
	
	/** 
	 * This method realizes the fire : for all the possibles transitions, it chooses one randomly and
	 * executes it.
	 */
	
	public void fire() throws LeavingArc, NegativeToken {
		LinkedList<Transition> transitionsTirable = new LinkedList<Transition>();
		for (Transition T : this.transitions) {
			if (T.isTirable()) {
				transitionsTirable.add(T);
			}
		}
		int i = (int)(Math.random() * (transitionsTirable.size()));
		transitionsTirable.get(i).realiseTransition();
	}	
}
