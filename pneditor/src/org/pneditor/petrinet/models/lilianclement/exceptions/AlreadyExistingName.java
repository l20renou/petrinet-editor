package  org.pneditor.petrinet.models.lilianclement.exceptions ;
;

/**
 * Return an exception if the user wants to create an object with the same name than an existing one
 * @author lilian
 * @param an object (either an arc, a place or a transition)
 * @raise an exception
 */


public class AlreadyExistingName extends Exception{
	public AlreadyExistingName(String message) {
		super(message);
	}
}
