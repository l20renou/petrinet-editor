package org.pneditor.petrinet.models.lilianclement.exceptions;

public class NegativeWeight extends Exception{
	public NegativeWeight(String message) {
		super(message);
	}
}
