package org.pneditor.petrinet.models.lilianclement.exceptions;

/**
 * Return an exception if we test isPossible for a sorting arc
 * @author lilian
 * @param an arc a
 * @raise an exception
 */

public class LeavingArc extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public LeavingArc() {
		System.out.println("This arc is a sorting arc, isPossible on it makes no sense");
	}
}
