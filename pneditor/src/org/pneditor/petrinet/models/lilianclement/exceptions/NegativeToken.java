package org.pneditor.petrinet.models.lilianclement.exceptions;

public class NegativeToken extends Exception{
	private String message; 

	public NegativeToken(String message) {
		super(message);
	}
	
	
	public String getMessage () {
		return message; 
	}
}
