package org.pneditor.petrinet.models.lilianclement.exceptions;


/**
 * Return an exception if the user wants to get something (a Transition, an Arc, a Place) not in the Petri diagram
 * @author lilian
 *
 */

public class DoesNotExist extends Exception {
	
	private String message; 
	
	public DoesNotExist(String message) {
		super(message);
	}
	
	public String getMessage () {
		return message; 
	}
}
