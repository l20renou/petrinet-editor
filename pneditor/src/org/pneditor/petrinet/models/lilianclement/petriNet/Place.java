package org.pneditor.petrinet.models.lilianclement.petriNet;
import org.pneditor.petrinet.models.lilianclement.exceptions.AlreadyExistingName;
import org.pneditor.petrinet.models.lilianclement.exceptions.DoesNotExist;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeToken;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeWeight;
import org.pneditor.petrinet.models.lilianclement.exceptions.LeavingArc;


public class Place {
	
	private String name;
	private int nbToken;
	
	
	public Place (String name, int nbToken) throws NegativeToken{
		this.name = name;
		if (nbToken<0) {
			throw new NegativeToken("The number of tokens can't be negative");
		}
		this.nbToken = nbToken;
	}
	
	public int getToken () {
		return this.nbToken ; 
	}
	
	public void setNbToken(int newNbToken) throws NegativeToken {
		if (newNbToken <0) {
			throw new NegativeToken("The number of token can't be negative");
		}
		this.nbToken = newNbToken;
	}
	
	public String getName() {
		return this.name;
	}
	
	/**
	 * Implements the method equals. It only verify the name, as we assume that two differents objects
	 * will not have the same name.
	 */
	
	public boolean equals(Object o){
		if (o == null ||!(o instanceof Place)) return false;
		Place p = (Place) o;	
		return(this.name == p.getName());
	}
	
	public String toString() {
		String newLine = System.getProperty("line.separator");
		return ("Name :" + this.name +","+ " Number of tokens :" + this.nbToken + newLine);
	}
}