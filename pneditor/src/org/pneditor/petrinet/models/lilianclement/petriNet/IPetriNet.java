package org.pneditor.petrinet.models.lilianclement.petriNet;
import java.util.LinkedList;

import org.pneditor.petrinet.models.lilianclement.exceptions.AlreadyExistingName;
import org.pneditor.petrinet.models.lilianclement.exceptions.DoesNotExist;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeToken;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeWeight;
import org.pneditor.petrinet.models.lilianclement.exceptions.LeavingArc;

public interface IPetriNet {
		
	
	public void addTransition (String name) throws AlreadyExistingName ;
	
		
	public void addPlace(String name, int nbToken) throws AlreadyExistingName, NegativeToken ;
	
	/**
	 * Method which add an arc to the list of already existing arcs 
	 * and also add it to the list of arc in the transition it is linked with 
	 * if the weight is zero, the arc will be an ArcZero
	 * if the weight is -1, the arc will be an ArcVideur
	 * @param arc
	 * @throws NegativeWeight 
	 */
	
	public void addArc (String name, int weight, boolean isEntering, Transition transition, Place place) throws AlreadyExistingName, DoesNotExist, NegativeWeight ;
	
	/**
	 * Method which returns the list of arcs
	 * @return LinkedList<Arc>
	 */
	
	public LinkedList<Arc> getArcs();
	
	/**
	 * Method which returns the list of places
	 * @return LinkedList<Place>
	 */
	
	public LinkedList<Place> getPlaces();
	
	/**
	 * Method which returns the list of transitions
	 * @return LinkedList<Transition>
	 */
	
	public LinkedList<Transition> getTransitions();
	
	/**
	 * Method which get the transition with the transition with the name in argument
	 * @throws DoesNotExist
	 */
	
	public Transition getTransition(String name) throws DoesNotExist;
	
	/**
	 * Method which get the place with the place with the name in argument
	 * @throws DoesNotExist
	 */
	
	public Place getPlace(String name) throws DoesNotExist;
	
	/**
	 * Method which get the arc with the arc with the name in argument
	 * @throws DoesNotExist
	 */
	
	public Arc getArc(String name) throws DoesNotExist;
	
	/**
	 * Allows to set the number of tokens in a given place
	 * @param newNbTokens
	 * @param place
	 * @throws NegativeToken 
	 */
	
	public void setNbTokens(int newNbTokens, Place place) throws DoesNotExist, NegativeToken;
	
	/**
	 * This method allows to set the weight of an arc, gives an exception if the arc doesn't exist
	 * @param weight
	 * @param arc
	 * @throws DoesNotExist
	 * @throws NegativeWeight 
	 */
	
	public void setWeight(int weight, Arc arc) throws DoesNotExist, NegativeWeight ;
	
	/**
	 * This method allows to delete an arc, gives an exception if the arc doesn't exist. 
	 * @param place
	 * @throws DoesNotExist
	 */

	public void removeArc(Arc arc) throws DoesNotExist;
	
	/**
	 * This method allows to delete a transition.
	 * It also deletes all the arcs which were linked to this transition.
	 * @param place
	 * @throws DoesNotExist
*	 */
	
	
	public void removeTransition (Transition transition) throws DoesNotExist ;
	
	/**
	 * This method allows to delete a place.
	 * It also delete all the arcs which were linked to this place
	 * and it deletes the arcs which are prensent in the list of arcs known by transition 
	 * @param place
	 * @throws DoesNotExist
	 */
	
	public void removePlace (Place place) throws DoesNotExist;
	
	/** 
	 * This method realizes the fire : for all the possibles transitions, it chooses one randomly and
	 * executes it.
	 * @throws LeavingArc
	 */
	
	public void fire() throws LeavingArc, NegativeToken;
}
