package org.pneditor.petrinet.models.lilianclement.petriNet;

import org.pneditor.petrinet.models.lilianclement.exceptions.AlreadyExistingName;
import org.pneditor.petrinet.models.lilianclement.exceptions.DoesNotExist;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeToken;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeWeight;
import org.pneditor.petrinet.models.lilianclement.exceptions.LeavingArc;


public class Arc {
	
	private int weight;
	private boolean isEntering; //true if the arc enters the transition, false if it gets out of the transition
	private Transition transition;
	private Place place;
	private String name;
	
	
	public Arc (String name, int weight, boolean isEntering, Transition transition, Place place) throws NegativeWeight{
		
		if (weight < -2) {
			throw new NegativeWeight("The weight can't be negative, except -1 which describes a reset Arc");
		}
		this.weight = weight;
		this.isEntering = isEntering;
		this.transition = transition;
		this.place = place;
		this.name = name;
	}
	
	public int getWeight () {
		return this.weight; 
	}
	
	public void setWeight(int newWeight) throws NegativeWeight {
		if (newWeight < -2) {
			throw new NegativeWeight("The weight can't be negative, except -1 which describes a reset Arc");
		}
		this.weight=newWeight;
	}
	
	/**
	 * This method gives the type of an Arc : true if the Arc is entering in the place, false if not.
	 * @return a boolean
	 */
	
	public boolean getType () {
		return this.isEntering;
	}
	
	public Place getPlace() {
		return this.place;
	}
	
	public Transition getTransition() {
		return this.transition;
	}
	
	public String getName() {
		return this.name;
	}
	
	/**
	 * This method return true if the Transition is possible, and false if not
	 * @return boolean
	 * @throws LeavingArc
	 */
	
	public boolean isPossible() throws LeavingArc {
		if (!this.isEntering) {
			throw new LeavingArc();
		}
		else {
			System.out.println(this.place.getToken()>=this.weight);
			return this.place.getToken()>=this.weight;	
		}
	}
	
	/**
	 * This method implements the equals function. We only test the name, assuming 
	 * that two Arcs won't have the same name.
	 */
	
	public boolean equals(Object o){
		if (o == null ||!(o instanceof Arc)) return false;
		Arc a = (Arc) o;	
		return(this.name.equals(a.getName()));
	}
	
	/**
	 * Makes the transition for one arc, assuming that this one is possible
	 * @throws LeavingArc 
	 */
	
	public void changeToken1Arc () throws NegativeToken {
		if (this.getType()) {
			// true means that the arc is getting out of the place
			this.place.setNbToken(this.place.getToken() - this.weight); 
		}
		
		else {
			// false means that the arc enters the place
			this.place.setNbToken(this.place.getToken() + this.weight);
		}
	}
	
	/**
	 * The classical method toString
	 */

	public String toString() {
		String newLine = System.getProperty("line.separator");
		if (this.isEntering == true) {
			return ("Name : " + this.name + ", Weight : "+ this.weight + ", Type : Entering" + ", Transition : " + this.transition.getName() + ", Place : " + this.place.getName() + newLine);  

		}
		else {
			return ("Name : " + this.name + ", Weight : "+ this.weight + ", Type : Leaving" + ", Transition : " + this.transition.getName() + ", Place : " + this.place.getName() + newLine);  

		}
	}
}