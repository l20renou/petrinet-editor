package org.pneditor.petrinet.adapters.lilianclement;

import java.util.LinkedList;

import org.pneditor.petrinet.AbstractArc;
import org.pneditor.petrinet.AbstractNode;
import org.pneditor.petrinet.AbstractPlace;
import org.pneditor.petrinet.AbstractTransition;
import org.pneditor.petrinet.PetriNetInterface;
import org.pneditor.petrinet.ResetArcMultiplicityException;
import org.pneditor.petrinet.UnimplementedCaseException;
import org.pneditor.petrinet.models.lilianclement.petriNet.Transition;
import org.pneditor.petrinet.models.lilianclement.petriNet.Place;
import org.pneditor.petrinet.models.lilianclement.petriNet.Arc;
import org.pneditor.petrinet.models.lilianclement.petriNet.PetriNet;


import org.pneditor.petrinet.models.lilianclement.exceptions.AlreadyExistingName;
import org.pneditor.petrinet.models.lilianclement.exceptions.DoesNotExist;
import org.pneditor.petrinet.models.lilianclement.exceptions.LeavingArc;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeToken;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeWeight;


public class PetriNetAdapter extends PetriNetInterface {

	// we create a new attribute net which is a PetriNet of our model 
	private PetriNet net; 
	// in our model the places, the arcs and the transtions have
	//names which help us to select an item to remove easily for example 
	private int placeNumber; 
	private int transitionNumber; 
	private int arcNumber; 

	
	public PetriNetAdapter() {
		// we create the new net with our model 
		this.net = new PetriNet("net"); 
		this.placeNumber = 0; 
		this.transitionNumber = 0; 
		this.arcNumber = 0;
		//little test to be sure that it is our model which is started
        System.out.println("Model of Clément and Lilian is launched (have fun ;=) !! )");
	}
	
	
	////////////////////////////////////////////////
	@Override
	/**
	 *This method adds a place 
	 */
	public AbstractPlace addPlace() {
		// To give a name to the place we use the attribute placeNumber which is increased each time we create a place
		// Like that we are sure it is unique
		placeNumber++; 
		AbstractPlace abstractPlace = null;
		try {
			net.addPlace(placeNumber+"", 0); 
		}
		catch (AlreadyExistingName e) {
		} catch (NegativeToken e) {
			e.getMessage();
		}
		try {
			abstractPlace = new PlaceAdapter(net.getPlace(placeNumber+"").getName(), 0, net);
		} catch (DoesNotExist e) {
			e.getMessage();
		}
		return abstractPlace; 
	}

	
	////////////////////////////////////////////////
	@Override
	
	/**
	 *This method adds a transition 
	 */
	
	public AbstractTransition addTransition()  {
		//to give a name to the place we use the attribute transitionNumber which is increased each time we create
		//a transition, like that we are sure it is unique
		transitionNumber++; 
		try {
			net.addTransition(transitionNumber+""); 
		}
		catch (AlreadyExistingName e) {
		}
		AbstractTransition abstractTransition = null;
		try {
			abstractTransition = new TransitionAdapter( net.getTransition(transitionNumber+"").getName());
		} catch (DoesNotExist e) {
			e.getMessage();
		}
		return abstractTransition;
	}

	////////////////////////////////////////////////
	@Override
	/**
	 * This method adds a regular arc  	
	 * @param AbstractNode source
	 * @param AbstractNode destination
	 * @throws UnimplementedCaseException
	 */

	public AbstractArc addRegularArc(AbstractNode source, AbstractNode destination) throws UnimplementedCaseException {
		
		// we create the list of all the transitions of the net 
		LinkedList<Transition> transitions = new LinkedList(); 
		transitions = net.getTransitions();
		// we create the list of all the place of the net 
		LinkedList<Place> places = new LinkedList(); 
		places = net.getPlaces(); 
		
		
		arcNumber++; 
		boolean isEntering = true; 
		// we initialise the Abstract Arc which will be returned 
		AbstractArc abstractArc = null; 
		
		// as source and destination are AbstractNode, we can't use them directly in our model because we don't 
		// know which of them is a place and which is a transition. An to create an Arc whith our model we need 
		// the transition and the place
		
		
		if (destination instanceof AbstractPlace) {
			// if the destination is a place, the Arc has to be a LEAVING one (he leaves from the transition
			isEntering = false; 
			
			// we create the transition which will be affected to the arc 
			Transition sourceReelle = null;
			for (int i =0 ; i<transitions.size(); i++) {
				if (transitions.get(i).getName() == source.getLabel()) {
					sourceReelle = transitions.get(i);
				}
			}
			
			// we create the place that will be affected to the arc 
			Place destinationReelle= null;
			for (int i =0 ; i<places.size(); i++) {
				if (places.get(i).getName() == destination.getLabel()) {
					destinationReelle = places.get(i);
					}
				}
			
				try {
					net.addArc(arcNumber +"", 1, isEntering, sourceReelle, destinationReelle);

				} catch (AlreadyExistingName | DoesNotExist | NegativeWeight e) {
					e.getMessage();
				}
				abstractArc = new ArcAdapter(arcNumber +"", 1, isEntering, sourceReelle, destinationReelle, net);
			}
		
		else {
			// the destination is a transition, the Arc has to be a ENTERING one (he enters in a transition)
			
			// we create the transition which will be affected to the arc 
			Transition destinationReelle = null;
			for (int i =0 ; i<transitions.size(); i++) {
				if (transitions.get(i).getName() == destination.getLabel()) {
					destinationReelle = transitions.get(i);
				}
			}
			
			// we create the place which will be affected to the arc 
			Place sourceReelle = null;
			for (int i = 0 ; i<places.size(); i++) {
				if (places.get(i).getName() == source.getLabel()) {
					sourceReelle = places.get(i);
					}
				}
			
				try {
					net.addArc(arcNumber +"", 1, isEntering, destinationReelle, sourceReelle);
				} 
				catch (AlreadyExistingName | DoesNotExist | NegativeWeight e) {
					e.getMessage();
				}
				abstractArc = new ArcAdapter(arcNumber +"", 1, isEntering, destinationReelle, sourceReelle, net);
			}	
		
		return abstractArc ;
	}

	
	///////////////////////////////////////////////
	@Override
	/**
	 * This method adds a inhibitory arc 
	 * @param AbstractNode source
	 * @param AbstractNode destination
	 */
	//the Inhibitory Arc is the Arc Zero of our model 
	public AbstractArc addInhibitoryArc(AbstractPlace place, AbstractTransition transition)
			throws UnimplementedCaseException {
		arcNumber++; 
		boolean isEntering = true; 
		
		AbstractArc abstractArc = null; 
		LinkedList<Transition> transitions = new LinkedList(); 
		transitions = net.getTransitions();
		
		LinkedList<Place> places = new LinkedList(); 
		places = net.getPlaces(); 
		
		// here we have only one case as an Inhibitory arc can only be an entering arc 
		// the destination is a transition, the Arc has to be a ENTERING one (he enters in a transition)
		Transition destinationReelle = null;
		for (int i =0 ; i<transitions.size(); i++) {
			// for all the transitions which are present in the net we are looking for the one  
			// which has the same name as the label of the transition given in an argument 
			if (transitions.get(i).getName() == transition.getLabel()) {
				destinationReelle = transitions.get(i);
			}
		}
		
		Place sourceReelle= null;
		for (int i =0 ; i<places.size(); i++) {
			// for all the places which are present in the net we are looking for the one  
			// which has the same name as the label of the place given in an argument 
			if (places.get(i).getName() == place.getLabel()) {
				sourceReelle = places.get(i);
				}
			}
		
		try {
			// we add the arc to our petrinet 
			net.addArc(arcNumber +"", 0, isEntering, destinationReelle, sourceReelle);
		} 
		catch (AlreadyExistingName | DoesNotExist | NegativeWeight e) {
			// all the exceptions of our model of petrinet 
			e.getMessage();
		}
		abstractArc = new ArcAdapter(arcNumber +"", 0, isEntering, destinationReelle, sourceReelle, net);	
		return abstractArc;
	}

	////////////////////////////////////////////////
	@Override
	/**
	 * This method add a reset arc 
	 * @param AbstractNode source
	 * @param AbstractNode destination
	 */
	//the Reset Arc is the Arc Videur
	public AbstractArc addResetArc(AbstractPlace place, AbstractTransition transition)
			throws UnimplementedCaseException {
		
		arcNumber++; 
		boolean isEntering = true; 
		AbstractArc abstractArc = null; 
		
		LinkedList<Transition> transitions = new LinkedList(); 
		transitions = net.getTransitions();
		
		LinkedList<Place> places = new LinkedList(); 
		places = net.getPlaces(); 
		
		// here we have only one case as a Reset arc can only be an entering arc 
		// the destination is a transition, the Arc has to be a ENTERING one (he enters in a transition)
		Transition destinationReelle = null;
		for (int i =0 ; i<transitions.size(); i++) {
			if (transitions.get(i).getName() == transition.getLabel()) {
				destinationReelle = transitions.get(i);
			}
		}
		
		Place sourceReelle= null;
		for (int i =0 ; i<places.size(); i++) {
			if (places.get(i).getName() == place.getLabel()) {
				sourceReelle = places.get(i);
				}
			}
		try {
			net.addArc(arcNumber +"", 10000, isEntering, destinationReelle, sourceReelle);
		} 
		catch (AlreadyExistingName | DoesNotExist | NegativeWeight e) {
			e.getMessage();
		}

		// we had to adapt our model, in which  a Reset Arc have a weight of -1 and an 
		// error only raise when the weight is <-1, but the model here doesn't like  when
		// there are negative weight. So we made the choice to put it to 1000 for a Reset
		abstractArc = new ArcAdapter(arcNumber +"", 10000, isEntering, destinationReelle, sourceReelle, net);	
		return abstractArc;
	}

	///////////////////////////////////////////////
	@Override
	/**
	 * This method removes a place 
	 * @param AbstractPlace place
	 */
	public void removePlace(AbstractPlace place) {
		LinkedList<Place> places = new LinkedList(); 
		places = net.getPlaces(); 
		
		LinkedList<Arc> arcs = new LinkedList(); 
		arcs = net.getArcs(); 
		
		for (int i =0 ; i<places.size(); i++) {
			// for all the places which are present in the net we are looking for the one  
			// which has the same name as the label of the place given in an argument and 
			// then we use the method remove from our model to remove it from the net. 
			if (places.get(i).getName() == place.getLabel()) {
				try {
					net.removePlace(places.get(i));
				}
				catch (DoesNotExist e) {
					e.getMessage();
				}
			}
			for(Arc arc : arcs) {
				if (places.get(i)== arc.getPlace()) {
					try {
						net.removeArc(net.getArc(arc.getName()));
					} catch (DoesNotExist e) {

						e.getMessage();
	
		
					}
				}
			}
			
		}
		
	}

	////////////////////////////////////////////////
	
	
	
	@Override
	/**
	 * This method removes a transition
	 * @param AbstractTransition transition
	 */
	public void removeTransition(AbstractTransition transition) {
		LinkedList<Transition> transitions = new LinkedList(); 
		transitions = net.getTransitions(); 
		
		LinkedList<Arc> arcs = new LinkedList(); 
		arcs = net.getArcs(); 
		
		for (int i =0 ; i<transitions.size(); i++) {
			// for all the transitions which are present in the net we are looking for the one  
			// which has the same name as the label of the transition given in an argument and 
			// then we use the method remove from our model to remove it from the net. 
			if (transitions.get(i).getName() == transition.getLabel()) {
				try {
					net.removeTransition(transitions.get(i));
				}
				catch (DoesNotExist e) {
					e.getMessage();
				}
			}
			for(Arc arc : arcs) {
				if (transitions.get(i)== arc.getTransition()) {
					try {
						net.removeArc(arc);
					} catch (DoesNotExist e) {
						e.getMessage();
		
					}
				}
			}
			
			
		}
		
		
	}		
	
 
	////////////////////////////////////////////////
	@Override
	/**
	 * This method removes an arc
	 * @param AbstractArc Arc
	 */
	public void removeArc(AbstractArc arc) {
		boolean isEntering = true; 
		LinkedList<Arc> arcs = new LinkedList(); 
		arcs = net.getArcs(); 
		
		// we have to know which one is place to use the right method to compare the two label 
		// of the transition and the place and remove the right Arc from net (with our method)
		// remove from our model 
		if (arc.getDestination() instanceof AbstractPlace) {
			isEntering = false ;
			
			// we get the place and the transition which are linked to this arc
			String placeId = arc.getDestination().getLabel();
			String transitionId = arc.getSource().getLabel();

			for (int i =0 ; i<arcs.size(); i++) {
				if (arcs.get(i).getTransition().getName() == transitionId 
						&& arcs.get(i).getPlace().getName()== placeId) {
					// we search for the arc in the net which link the same place and transition
					// as it is not allowed to have two arc which linked the same items. 
					try {
						net.removeArc(net.getArc((arcs.get(i)).getName()));
					} catch (DoesNotExist e) {
						e.getMessage();
					}
				}
		}
		
		}
		else {
			isEntering = false ;
			String placeId = arc.getSource().getLabel();
			String transitionId = arc.getDestination().getLabel();
			for (int i =0 ; i<arcs.size(); i++) {
				if (arcs.get(i).getTransition().getName() == transitionId 
						&& arcs.get(i).getPlace().getName()== placeId) {
					try {
						net.removeArc(arcs.get(i));
					} catch (DoesNotExist e) {
						e.getMessage();
					}
				}
			}
		}
	}

	
	////////////////////////////////////////////////
	@Override
	/**
	 * This method returns true if a transition is tirable 
	 * @param AbstractTransition transition
	 * @throws ResetArcMultiplicityException
	 */
	public boolean isEnabled(AbstractTransition transition) throws ResetArcMultiplicityException {
		LinkedList<Transition> transitions = new LinkedList(); 
		transitions = net.getTransitions(); 
		System.out.println(net.getPlaces());
		System.out.println(net.getArcs());
		
		LinkedList<Arc> arcs = new LinkedList(); 
		boolean condition = false; 
		LinkedList<Boolean> conditions = new LinkedList(); 

		for (int i =0; i<net.getArcs().size(); i++) {
			if (net.getArcs().get(i).getTransition().getName() == transition.getLabel()) {
				arcs.add(net.getArcs().get(i));
			}
		}
		

		for (int j =0; j<arcs.size(); j++) {
		
			if (arcs.get(j).getWeight() == 10000) {
				if (arcs.get(j).getPlace().getToken() ==0) {
					condition = false; 
					conditions.add(condition);
				}
				else {
					condition = true; 
					conditions.add(condition); 
				}

				
			}
			
			else if (arcs.get(j).getWeight() == 0) {
				if (arcs.get(j).getPlace().getToken() !=0) {
					condition = false; 
					conditions.add(condition);
				}
				else {
					condition = true; 
					conditions.add(condition);
					try {
						arcs.get(j).getPlace().setNbToken(0);
					} catch (NegativeToken e) {
						e.getMessage();
					}
				}
				
			}
			
			
			
			else {
				try {
					condition = arcs.get(j).isPossible();
					conditions.add(condition); 
				} catch (LeavingArc e) {
					e.getMessage();
				} 
	
			}
		}
		
		for (int j =0; j<conditions.size(); j++)
			if (conditions.get(j) == false) {
				return false; 
			}
			
		return true; 
			
	}

	////////////////////////////////////////////////
	@Override
	/**
	 * This method fires the whole petrinet, we could use the method fire of our petrinet as 
	 * it was a stochastic fire which fire a random transition 
	 * @param AbstractTransition transition
	 * @throws ResetArcMultiplicityException
	 */
	public void fire(AbstractTransition transition) throws ResetArcMultiplicityException {
		LinkedList<Transition> transitions = new LinkedList();
		transitions = net.getTransitions(); 
		LinkedList<Arc> arcsnet = new LinkedList();
	    arcsnet = net.getArcs(); 
		for (Arc arc : arcsnet) {
			if (arc.getTransition().getName() == transition.getLabel() ) {
				if (arc.getWeight() ==10000) {
					try {
						arc.getPlace().setNbToken(0);
					} catch (NegativeToken e) {
						e.getMessage();
					}
				}
				else {
					try {
						arc.changeToken1Arc();
					} catch (NegativeToken e) {
						e.getMessage();
					}
				}
			}
		}
			

		}	
	}
