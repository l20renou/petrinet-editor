package org.pneditor.petrinet.adapters.lilianclement;

import org.pneditor.petrinet.AbstractPlace;
import org.pneditor.petrinet.models.lilianclement.petriNet.Place;
import org.pneditor.petrinet.models.lilianclement.exceptions.DoesNotExist;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeToken;
import org.pneditor.petrinet.models.lilianclement.petriNet.PetriNet;

public class PlaceAdapter extends AbstractPlace{
	
	private Place place; 
	private PetriNet net; 

	/**
	 * contructor which creates an abstract place 
	 * @param label
	 * @param nbTokens
	 */
	public PlaceAdapter(String label, int nbTokens, PetriNet net) {
		super(label);
		this.net = net;
		try {
			this.place = net.getPlace(label);
		} catch (DoesNotExist e) {
			e.getMessage();
		}
	}	

	
	
	@Override
	public void addToken() {
		int token = place.getToken(); 
		try {
			//we add one token to the already existing number of tockens
			place.setNbToken(token+1);
			System.out.println("On a ajouté un jeton");
		} catch (NegativeToken e) {
			e.getMessage();
		}
	}

	@Override
	public void removeToken() {
		int token = place.getToken(); 
		try {
			//we remove one token to the already existing number of tockens
			place.setNbToken(token-1);
		} catch (NegativeToken e) {
			e.getMessage();
		}
	}

	
	public String getLabel() {
		return place.getName();
	}
	
	@Override
	public int getTokens() {
		return place.getToken();
	}

	@Override
	public void setTokens(int tokens) {
		try {
			place.setNbToken(tokens);
			System.out.println("on a modifié des jetons");
			System.out.println(place.getToken());
		} catch (NegativeToken e) {
			e.getMessage();
		}		
	}

}
