package org.pneditor.petrinet.adapters.lilianclement;

import org.pneditor.petrinet.AbstractArc;
import org.pneditor.petrinet.AbstractNode;
import org.pneditor.petrinet.AbstractPlace;
import org.pneditor.petrinet.AbstractTransition;
import org.pneditor.petrinet.ResetArcMultiplicityException;
import org.pneditor.petrinet.models.lilianclement.petriNet.Arc;
import org.pneditor.petrinet.models.lilianclement.petriNet.PetriNet;
import org.pneditor.petrinet.models.lilianclement.petriNet.Place;
import org.pneditor.petrinet.models.lilianclement.petriNet.Transition;
import org.pneditor.petrinet.models.lilianclement.exceptions.DoesNotExist;
import org.pneditor.petrinet.models.lilianclement.exceptions.NegativeWeight;

public class ArcAdapter extends AbstractArc{

	private Arc arc; 
	private PetriNet net; 

	
	/**
	 * Constructor which creates an abstract arc 
	 * @param name
	 * @param weight
	 * @param isEntering
	 * @param transition
	 * @param place
	 */
	public ArcAdapter (String name, int weight, boolean isEntering, Transition transition, Place place, PetriNet net) {
		this.net = net;
		try {
			this.arc = new Arc ( name,  weight,  isEntering,  transition,  place) ;
		} catch (NegativeWeight e) {
			e.getMessage();
		}
	}

	/**
	 * Method to get the name of the arc
	 * @return String 
	 */
	public String getLabel (){
	   return this.arc.getName(); 
	}
	   
	@Override
	/**
	 * Method to get the source of an arc, it can be either a transition or a place
	 * @return abstractTransition
	 * @return abstractplace
	 */
	public AbstractNode getSource() {
		if (this.arc.getType() == true) {
			// if the arc type is true the arc is ENTERING and the source correspond to its place 
			AbstractPlace abstractplace = new PlaceAdapter (this.arc.getPlace().getName(), this.arc.getPlace().getToken(),net); 
			return abstractplace;
		}
		else {
			// else the arc type is false the arc is LEAVING and the source correspond to its transition 
			AbstractTransition abstractTransition= new TransitionAdapter(this.arc.getTransition().getName()); 
			return abstractTransition;
		}
	}

	@Override
	/**
	 * Method to get the destination of an arc, it can be either a transition or a place
	 * @return abstractTransition
	 * @return abstractplace
	 */
	public AbstractNode getDestination() {
		if (this.arc.getType() == false) {
			// if the arc type is true the arc is LEAVING and the destination correspond to its place 
			AbstractPlace abstractplace = new PlaceAdapter (this.arc.getPlace().getName(), this.arc.getPlace().getToken(), net); 
			return abstractplace;
		}
		else {
			// if the arc type is true the arc is ENTERING and the source correspond to its transition 
			AbstractTransition abstractTransition= new TransitionAdapter(this.arc.getTransition().getName()); 
			return abstractTransition;
		}
	}

	@Override
	/**
	 * return true if it is a reset arc
	 * @return boolean 
	 */
	public boolean isReset() {
		// we had to adapt our model, in which  a Reset Arc have a weight of -1 and an 
		// error only raise when the weight is <-1, but the model here doesn't like  when
	    // there are negative weight. So we made the choice to put it to 10000 for a Reset
		return (this.arc.getWeight()==10000);
	}

	@Override
	/**
	 * return true if it is a regular arc
	 * @return boolean 
	 */
	public boolean isRegular() {
		return ((this.arc.getWeight()>0) && (this.arc.getWeight()!=10000));
	}

	@Override
	/**
	 * return true if it is a inhibitory arc
	 * @return boolean 
	 */
	public boolean isInhibitory() {
		return (this.arc.getWeight()==0);
	}

	@Override
	/**
	 * return the weight of an arc 
	 * @return int
	 */
	public int getMultiplicity() throws ResetArcMultiplicityException {
		try {
			return net.getArc(this.arc.getName()).getWeight();
		} catch (DoesNotExist e) {
			
		}
		return 0;
	}

	@Override
	/**
	 * set the weight of an arc 
	 */
	public void setMultiplicity(int multiplicity) throws ResetArcMultiplicityException {
		try {
			try {
				net.getArc(this.arc.getName()).setWeight(multiplicity);
			} catch (DoesNotExist e) {
				e.getMessage();
			}
			System.out.println("On a changé le poids");
			System.out.println(this.arc.toString());
		} catch (NegativeWeight e) {
			e.getMessage();
		}
		
	}
	/**
	 * method to string 
	 */
	public String toString () {
		return this.arc.toString();
	}
	

}
